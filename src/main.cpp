#include <iostream>

#include "Matrix.hpp"
#include "RandomGenerator.hpp"
#include "RectangleAlgorithm.hpp"

using namespace op;

int main(int argc, char** argv)
{
    const int N = 1'000;

    RandomGenerator gen(0, N - 1);
    Matrix<int> m(N, N);

    for (int i = 0; i < 5 * N * N; i++)
    {
        int row = gen.Generate();
        int col = gen.Generate();

        m(row, col) = 0; // 0 - point, 1 - no point
    }

    auto maxRect = FindLargestRectangleInMatrix(m);
    std::cout << maxRect << std::endl;

    return 0;
}