Author: Oleh Pomazan oleh.pomazan@gmail.com

## Problem
Generate 5,000,000 points on a plane where X and Y are integers and belong to the range [1; 1000].
Develop an algorithm that determines a rectangle of a maximum area without any points.

## Restriction notes

Restrictions on the rectangle position:
1) Rectangle vertices have integer coordinates
2) Rectangle sides are parallel to coordinate axes
3) Rectangle does not contain points inside and on its sides

Problem is equivalent to finding the largest non-zero rectangle sub-matrix in a binary matrix.
Generated points are zeros, locations without points are ones.
E.g. matrix 
```
[ 0 1 1
  0 1 1
  0 0 0] 
```
has sub-matrix 2x2 which is the same as rectangle 2x2 without points on 3x3 field.

## Algorithm description
1) Generate a helper matrix according to the rule:
    - if `M[i, j] != 0` then `M[i, j] += M[i - 1, j]`
Each matrix element of helper matrix contains the height of the upper column rectangle.

E.g. for matrix
```
[ 0 1 1
  0 1 1
  0 0 1
]
```

helper matrix is
```
[ 0 1 1
  0 2 2
  0 0 3
]
```

2) For each row in helper matrix:
        Find maximal area rectangle for this row
   Find maximal area rectangle for all rows

For 1st row max area is 2:
```
+--------+
| 0| 1| 1|
+--------+
```

For 2nd row - 4:
```
   +--+--+
   |  |  |
+--------+
| 0| 2| 2|
+--------+
```

For 3rd row - 3:
```
      +--+
      |  |
   +-----+
   |  |  |
+--------+
| 0| 2| 3|
+--------+
```

The most tricky part is to find max area rectangle for given row. 

There are 3 approaches:
1) Calculate all possible rectangles for each element of row O(n^2)
2) Divide and conquer approach O(n log(n)) https://www.geeksforgeeks.org/largest-rectangular-area-in-a-histogram-set-1/
3) Stack-based solution O(n) https://www.geeksforgeeks.org/largest-rectangle-under-histogram/

3rd approach is implemented in `FindLargerstRectangleForRow` function in RectangleAlgorithm.hpp 