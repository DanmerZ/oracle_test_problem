#pragma once

#include <vector>
#include <initializer_list>

namespace op
{

    template <typename T>
    class Matrix
    {
    public:
        using RowT = std::vector<T>;
        using RowsT = std::vector<RowT>;

        Matrix(size_t rows, size_t cols)
        {
            if (rows < 1 || cols < 1)
                throw std::runtime_error(
                    "Matrix should be at least 1x1 or larger.");

            rows_.resize(rows);
            for (auto& row : rows_) 
            {
                row.resize(cols, 1);
            }
        }

        Matrix(std::initializer_list<std::initializer_list<T>> lst)
            : Matrix(lst.size(), lst.size() ? lst.begin()->size() : 0)
        {
            int row = 0, col = 0;
            for (const auto& l : lst)
            {
                for (const auto& v : l)
                {
                    rows_[row][col] = v;
                    ++col;
                }
                col = 0;
                ++row;
            }
        }

        Matrix(const Matrix& other)
        {
            this->rows_ = other.rows_;
        }

        T operator()(T row, T col) const
        {
            return rows_.at(row).at(col);
        }

        int& operator()(T row, T col)
        {
            return rows_.at(row).at(col);
        }

        RowT GetRow(T row) const
        {
            return rows_.at(row);
        }

        RowT& GetRow(T row)
        {
            return rows_.at(row);
        }

        T Rows() const
        {
            return static_cast<T>(rows_.size());
        }

        T Cols() const
        {
            return static_cast<T>(rows_.at(0).size());
        }

    private:
        RowsT rows_;
    };

}
