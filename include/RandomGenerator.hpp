#pragma once

#include <random>

namespace op
{

    class RandomGenerator
    {
    public:
        explicit RandomGenerator(int minVal, int maxVal)
            : engine_(dev_()),
            dist_(minVal, maxVal)
        {
        }

        RandomGenerator(const RandomGenerator&) = delete;

        inline int Generate()
        {
            return dist_(engine_);
        }

    private:
        std::random_device dev_;
        std::default_random_engine engine_;
        std::uniform_int_distribution<int> dist_;
    };

}
