#pragma once

#include <deque>
#include <future>
#include <stack>

#include "Matrix.hpp"
#include "profile.h"
#include "Rectangle.hpp"

namespace op
{

    template <typename T>
    using Row = typename Matrix<T>::RowT;

    template <typename T>
    Rectangle<T> FindLargerstRectangleForRow(
        const Row<T>& row, T rowNumber)
    {
        if (row.empty())
            throw std::runtime_error("Empty row");

        Rectangle<T> resultRect;

        T maxArea = 0;
        std::deque<T> theRow(std::begin(row), std::end(row));
        theRow.push_back(-1);
        theRow.push_front(-1);

        std::stack<T> st;
        st.push(0);

        for (int i = 0; i < theRow.size(); i++)
        {
            while (theRow.at(i) < theRow.at(st.top()))
            {
                Rectangle<T> r;
                r.height_ = theRow.at(st.top());
                st.pop();

                r.width_ = i - st.top() - 1;
                int area = r.Area();
                maxArea = std::max(maxArea, area);

                r.topCornerX_ = rowNumber - r.height_ + 1;
                r.topCornerY_ = st.top();

                if (area == maxArea) {
                    resultRect = r;
                }
            }
            st.push(i);
        }

        return resultRect;
    }

    template <typename T>
    Rectangle<T> FindLargestRectangleInMatrix(const Matrix<T>& m)
    {
        LOG_DURATION("FindLargestRectangleInMatrix");

        Matrix<T> helperMatrix = m;

        for (int i = 1; i < m.Rows(); i++) 
        {
            for (int j = 0; j < m.Cols(); j++) 
            {
                if (helperMatrix(i, j) > 0) 
                {
                    helperMatrix(i, j) += helperMatrix(i - 1, j);
                }
            }
        }

        auto maxRect = FindLargerstRectangleForRow(helperMatrix.GetRow(0), 0);

        std::vector<std::future<Rectangle<T>>> futures;

        for (int i = 1; i < helperMatrix.Rows(); i++)
        {
            futures.emplace_back(
                std::async(
                    [&helperMatrix, i]() {
                        return FindLargerstRectangleForRow(
                            helperMatrix.GetRow(i), i); 
                    }
                )
            );
        }

        for (auto& f : futures) 
        {
            auto rect = f.get();
            if (rect.Area() > maxRect.Area())
            {
                maxRect = rect;
            }
        }

        return maxRect;
    }

}