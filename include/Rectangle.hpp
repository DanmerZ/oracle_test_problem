#pragma once

#include <iostream>
#include <tuple>

namespace op
{

    template <typename T>
    struct Rectangle
    {
        T topCornerX_ = 0, topCornerY_ = 0;
        T width_ = 0, height_ = 0;

        Rectangle() = default;

        Rectangle(T topX, T topY, T w, T h)
            : topCornerX_(topX), 
            topCornerY_(topY),
            width_(w),
            height_(h)
        {
        }

        T Area() const
        {
            return width_ * height_;
        }
    };

    template <typename T>
    inline bool operator==(const Rectangle<T>& lhs, const Rectangle<T>& rhs)
    {
        return std::tie(
            lhs.topCornerX_, lhs.topCornerY_, lhs.width_, lhs.height_) ==
            std::tie(
                rhs.topCornerX_, rhs.topCornerY_, rhs.width_, rhs.height_);
    }

    template <typename T>
    inline std::ostream& operator<<(std::ostream& out, const Rectangle<T>& r)
    {
        out << "Rectangle: x=" << r.topCornerX_ << ", y=" << r.topCornerY_
            << ", w=" << r.width_ << ", h=" << r.height_
            << ", area=" << r.Area();
        return out;
    }

}
