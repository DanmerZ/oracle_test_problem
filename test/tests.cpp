#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "Matrix.hpp"
#include "Rectangle.hpp"
#include "RectangleAlgorithm.hpp"

using namespace op;
using Rect = op::Rectangle<int>;

TEST_CASE("Matrix construction and initialization")
{
    Matrix<int> m(3, 4);
    REQUIRE(m.Rows() == 3);
    REQUIRE(m.Cols() == 4);
    REQUIRE(m.GetRow(0).size() == 4);
}

TEST_CASE("Matrix elements assignment")
{
    Matrix<int> m(3, 4);
    m(1, 1) = 2;
    REQUIRE(m(1, 1) == 2);
}

TEST_CASE("Matrix initializer list")
{
    Matrix<int> m = 
    {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };
    REQUIRE(m(0, 0) == 1);
    REQUIRE(m(1, 1) == 5);
    REQUIRE(m(2, 2) == 9);
}   

TEST_CASE("Rectangle area")
{
    Rect rect;
    rect.width_ = 3;
    rect.height_ = 4;
    REQUIRE(rect.Area() == 12);
}

TEST_CASE("FindLargerstRectangleForRow on different inputs")
{
    using Row = Matrix<int>::RowT;

    SECTION("Empty input") 
    {
        Row row;
        REQUIRE_THROWS_WITH(FindLargerstRectangleForRow(row, 0), "Empty row");        
    }

    SECTION(R"(1 element row)") 
    {
        REQUIRE(FindLargerstRectangleForRow({ 1 }, 0) == Rect{ 0,0,1,1 });
        REQUIRE(FindLargerstRectangleForRow({ 2 }, 1) == Rect{ 0,0,1,2 });
        REQUIRE(FindLargerstRectangleForRow({ 10 }, 9) == Rect{ 0,0,1,10 });
    }

    SECTION(R"(
    +---+                   +---+
    |   |   +---+   +---+   |   |
    |   |   |   +---+   |   |   |
    |   |   |   |   |   |   |   |
    |   |   |   |   |   |   |   |
    |   +---+   |   |   +---+   |
    |   |   |   |   |   |   |   |
    +---------------------------+
    | 6 | 2 | 5 | 4 | 5 | 2 | 6 |
    +---+---+---+---+---+---+---+
    )") 
    {
        Row row = { 6, 2, 5, 4, 5, 2, 6 };
        auto rect = FindLargerstRectangleForRow(row, 0);
        REQUIRE(rect == Rect{ -1, 0, 7, 2 });
        REQUIRE(rect.Area() == 14);
    }

    SECTION(R"(
    +---+                   +---+
    |   |   +---+   +---+   |   |
    |   |   |   +---+   |   |   |
    |   |   |   |   |   |   |   |
    |   |   |   |   |   |   |   |
    |   +---+   |   |   |   |   |
    |   |   |   |   |   +---+   |
    +---------------------------+
    | 6 | 2 | 5 | 4 | 5 | 1 | 6 |
    +---+---+---+---+---+---+---+
    )")
    {
        Row row = { 6, 2, 5, 4, 5, 1, 6 };
        auto rect = FindLargerstRectangleForRow(row, 0);
        REQUIRE(rect == Rect{ -3, 2, 3, 4 });
        REQUIRE(rect.Area() == 12);
    }
}

TEST_CASE("FindLargestRectangleInMatrix on different inputs")
{
    SECTION("One element matrix") 
    {
        Matrix<int> m = { { 1 } };
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 0, 0, 1, 1 });
        REQUIRE(rect.Area() == 1);
    }

    SECTION(R"( 1 0
                0 0)")
    {
        Matrix<int> m(2, 2);
        m(0, 0) = 1; m(0, 1) = 0;
        m(1, 0) = 0; m(1, 1) = 0;
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 0, 0, 1, 1 });
        REQUIRE(rect.Area() == 1);
    }

    SECTION(R"( 1 1
                0 0)")
    {
        Matrix<int> m(2, 2);
        m(0, 0) = 1; m(0, 1) = 1;
        m(1, 0) = 0; m(1, 1) = 0;
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 0, 0, 2, 1 });
        REQUIRE(rect.Area() == 2);
    }

    SECTION(R"( 1 1
                1 0)")
    {
        Matrix<int> m(2, 2);
        m(0, 0) = 1; m(0, 1) = 1;
        m(1, 0) = 1; m(1, 1) = 0;
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 0, 0, 2, 1 });
        REQUIRE(rect.Area() == 2);
    }

    SECTION(R"( 0 1
                1 0)")
    {
        Matrix<int> m(2, 2);
        m(0, 0) = 0; m(0, 1) = 1;
        m(1, 0) = 1; m(1, 1) = 0;
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 0, 1, 1, 1 });
        REQUIRE(rect.Area() == 1);
    }

    SECTION(R"( 0 1
                1 1)")
    {
        Matrix<int> m(2, 2);
        m(0, 0) = 0; m(0, 1) = 1;
        m(1, 0) = 1; m(1, 1) = 1;
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 1, 0, 2, 1 });
        REQUIRE(rect.Area() == 2);
    }

    SECTION(R"( 1 1
                1 1)")
    {
        Matrix<int> m(2, 2);
        m(0, 0) = 1; m(0, 1) = 1;
        m(1, 0) = 1; m(1, 1) = 1;
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 0, 0, 2, 2 });
        REQUIRE(rect.Area() == 4);
    }

    SECTION(R"( 0 0 0 0
                0 1 1 0
                0 1 1 0
                0 0 0 0)")
    {
        Matrix<int> m = 
        {
            { 0, 0, 0, 0 },
            { 0, 1, 1, 0 },
            { 0, 1, 1, 0 },
            { 0, 0, 0, 0 }
        };
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 1, 1, 2, 2 });
        REQUIRE(rect.Area() == 4);
    }

    SECTION(R"( 1 0 0 0
                0 1 1 0
                0 1 1 0
                0 0 0 0)")
    {
        Matrix<int> m =
        {
            { 1, 0, 0, 0 },
            { 0, 1, 1, 0 },
            { 0, 1, 1, 0 },
            { 0, 0, 0, 0 }
        };
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 1, 1, 2, 2 });
        REQUIRE(rect.Area() == 4);
    }

    SECTION(R"( 1 0 0 0
                1 1 1 0
                1 1 1 0
                0 0 0 0)")
    {
        Matrix<int> m =
        {
            { 1, 0, 0, 0 },
            { 1, 1, 1, 0 },
            { 1, 1, 1, 0 },
            { 0, 0, 0, 0 }
        };
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 1, 0, 3, 2 });
        REQUIRE(rect.Area() == 6);
    }

    SECTION(R"( 1 1 1 1
                1 1 1 1
                1 1 1 0
                0 0 0 0)")
    {
        Matrix<int> m =
        {
            { 1, 1, 1, 1 },
            { 1, 1, 1, 1 },
            { 1, 1, 1, 0 },
            { 0, 0, 0, 0 }
        };
        auto rect = FindLargestRectangleInMatrix(m);
        REQUIRE(rect == Rect{ 0, 0, 3, 3 });
        REQUIRE(rect.Area() == 9);
    }
}
